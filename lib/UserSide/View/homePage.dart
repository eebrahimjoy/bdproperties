import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ecommerce/UserSide/View/products/products.dart';
import 'package:flutter_ecommerce/sharedPreference.dart';

import 'Categories/categories.dart';
import 'cart/cart.dart';
import 'customAppBar.dart';
import 'customDrawer.dart';
import 'imageCarousel.dart';
import 'login.dart';

class HomePage extends StatefulWidget {
  String userName;
  String userEmail;
  HomePage({this.userName,this.userEmail});

@override
  _HomePageState createState() => _HomePageState(userName,userEmail);
}

class _HomePageState extends State<HomePage> {
  String userName;
  String userEmail;
  String role;
  bool visible;
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  _HomePageState(this.userName,this.userEmail);

  List<String> carouselImages = [];
  fetchCarouselImages()async{
      FirebaseFirestore _fireStore = FirebaseFirestore.instance;
      QuerySnapshot snap = await _fireStore.collection('Carousel-Image').get();
      setState(() {
        for(int i=0;i<snap.docs.length;i++){
          carouselImages.add(snap.docs[i]['caresoul-image']);
        }
      });

    }


  Future<void> checkAdmin() async {
    User user = _firebaseAuth.currentUser;
    final DocumentSnapshot snap = await FirebaseFirestore.instance.collection('user-data').doc(user.email).get();
    setState(() {
      userName = snap['name'];
      userEmail = user.email;
      role=snap['role'];
    });
    if(role=='admin'){
      setState(() {
        visible=true;

      });
    }else{
      visible = false;
    }
  }

   Future<void> _signOut() async{
     Prefs.clearPref();
     await _firebaseAuth.signOut();
     Navigator.of(context).pushAndRemoveUntil(
         MaterialPageRoute(builder: (context) => Login()),(route)=>false);
   }

   void setPref() async{
    await Prefs.loadPref();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setPref();
    checkAdmin();
    fetchCarouselImages();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(
        onPressedSearch: (){
          //Navigator.push(context, MaterialPageRoute(builder: (context)=>Cart()));
        },
        onPressedCart: (){
          Navigator.push(context, MaterialPageRoute(builder: (context)=>Cart()));
        }
      ),
      drawer: CustomDrawer(
        userName: userName,
        email: userEmail,
        visibile: visible,
        onPressed: (){
          _signOut();
        },
      ),
      body: Column(
        children: [
          ImageCarousel(carouselImages: carouselImages,),

          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("Categories"),
          ),

          Categories(),

          Padding(
            padding: const EdgeInsets.only(top: 25.0,left: 8.0, bottom: 15.0),
            child: Text("Recent Products"),
          ),

          Flexible(
            child: Products()
          )

        ],
      ),
    );
  }
}

