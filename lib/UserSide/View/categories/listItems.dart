import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CategoryListItems extends StatelessWidget {

  final String img;
  final String img_caption;

  CategoryListItems({
    this.img,
    this.img_caption
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: InkWell(
        onTap: (){},
        child: Container(
          width: 80.0,
          height: 80.0,
          child: ListTile(
            title: Image.asset(
                img,
                width: 80.0,
                height: 40.0
            ),
            subtitle: Container(
              alignment: Alignment.topCenter,
              child: Text(img_caption, style: TextStyle(fontSize: 11.0),)
            ),
          ),
        ),
      ),
    );
  }
}
