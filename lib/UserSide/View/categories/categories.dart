import 'package:flutter/cupertino.dart';
import 'package:flutter_ecommerce/UserSide/View/categories/listItems.dart';

class Categories extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60.0,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: [
          CategoryListItems(
            img: "images/category/tshirt.png",
            img_caption: "shirt"
          ),

          CategoryListItems(
              img: "images/category/dress.png",
              img_caption: "dress"
          ),

          CategoryListItems(
              img: "images/category/jeans.png",
              img_caption: "pants"
          ),

          CategoryListItems(
              img: "images/category/formal.png",
              img_caption: "formal"
          ),

          CategoryListItems(
              img: "images/category/informal.png",
              img_caption: "informal"
          ),

          CategoryListItems(
              img: "images/category/shoe.png",
              img_caption: "shoes"
          ),

          CategoryListItems(
              img: "images/category/accessories.png",
              img_caption: "others"
          )

        ],
      ),
    );
  }
}