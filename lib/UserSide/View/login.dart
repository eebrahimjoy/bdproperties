import 'package:animated_background/animated_background.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:edge_alert/edge_alert.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ecommerce/AdminPanel/admin_home_page.dart';
import 'package:flutter_ecommerce/UserSide/View/registration.dart';
import 'package:flutter_ecommerce/UserSide/commonwidgets/custombutton.dart';
import 'package:flutter_ecommerce/UserSide/commonwidgets/input_field.dart';
import 'package:flutter_ecommerce/sharedPreference.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'homePage.dart';


class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> with SingleTickerProviderStateMixin{
  final _auth = FirebaseAuth.instance;
  bool showSpinner;
  String email;
  String password;
  String role;
  String userName;

   void getUserName() async{
    User  user = FirebaseAuth.instance.currentUser;
    final DocumentSnapshot snap = await FirebaseFirestore.instance.collection('user-data').doc(user.email).get();
    setState(() {
      showSpinner=true;
      userName=snap['name'];

    });
  }


  void checkRole() async{
    User user = FirebaseAuth.instance.currentUser;
    final DocumentSnapshot snap = await FirebaseFirestore.instance.collection('user-data').doc(user.email).get();
    setState(() {
      role = snap['role'];
      showSpinner=true;
    });
    if (role == 'user'){
      Prefs.setBool(Prefs.IS_LOGGED_IN,true);
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (context) => HomePage(userName: userName,userEmail: user.email,)),
              (route) => false);
    }else if(role == 'admin'){
      Prefs.setBool(Prefs.IS_LOGGED_IN,true);
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (context) => AdminHomePage(adminName: userName,adminEmail: user.email,)),
              (route) => false);
    }
  }

  @override
  void initState() {
    showSpinner = false;
    super.initState();
    setPref();
  }

  void setPref() async{
     await Prefs.loadPref();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueAccent[50],
      body: AnimatedBackground(
        behaviour: BubblesBehaviour(),
        vsync: this,
        child: Container(
          alignment: Alignment.center,
          child: ModalProgressHUD(
            inAsyncCall: showSpinner,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 24.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Flexible(
                    child: Hero(
                      tag: 'logo',
                      child: Icon(
                        Icons.shopping_cart,
                        size: 120,
                        color: Colors.pink,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  Hero(
                    tag: 'HeroTitle',
                    child: Text(
                      'E-Shopping',
                      style: TextStyle(
                        decoration: TextDecoration.none,
                          color: Colors.pink,
                          fontFamily: 'Poppins',
                          fontSize: 26,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                  SizedBox(
                    height: 48.0,
                  ),
                  InputFiled(
                    hintText: 'Enter e-mail',
                    leading: Icons.mail,
                    color: Colors.pink,
                    keyboard: TextInputType.emailAddress,
                    obscure: false,
                    userTyped: (value) {
                      email = value;
                    },
                  ),
                  InputFiled(
                    hintText: 'Password',
                    leading: Icons.lock,
                    color: Colors.pink,
                    keyboard: TextInputType.visiblePassword,
                    obscure: true,
                    userTyped: (value) {
                      password = value;
                    },
                  ),
                  SizedBox(
                    height: 24.0,
                  ),
                  CustomButton(
                    onpress: () async {
                      if (password != null && email != null) {
                        setState(() {
                          showSpinner = true;
                        });
                        try {
                          final loggedUser = await _auth.signInWithEmailAndPassword(
                              email: email, password: password);
                          if (loggedUser != null) {
                            checkRole();
                            getUserName();
                            setState(() {
                              showSpinner = false;
                            });
                          }
                        } catch (e) {
                          setState(() {
                            showSpinner = false;
                          });
                          EdgeAlert.show(context,
                              title: 'Login Failed',
                              description: e.toString(),
                              gravity: EdgeAlert.BOTTOM,
                              icon: Icons.error,
                              backgroundColor: Colors.pink);
                        }
                      } else {
                        EdgeAlert.show(context,
                            title: 'Uh oh!',
                            description: 'Please enter the email and password.',
                            gravity: EdgeAlert.BOTTOM,
                            icon: Icons.error,
                            backgroundColor: Colors.pink);
                      }
                    },
                    text: 'Log in',
                    accentColor: Colors.white,
                    mainColor: Colors.pink,
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  GestureDetector(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                              return Registration();
                            }));
                      },
                      child: Text(
                        'or sign up instead',
                        style: TextStyle(
                            fontFamily: 'Poppins',
                            fontSize: 14,
                            color: Colors.blueAccent),
                      ))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

