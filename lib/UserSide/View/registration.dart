import 'package:animated_background/animated_background.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:edge_alert/edge_alert.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ecommerce/UserSide/commonwidgets/custombutton.dart';
import 'package:flutter_ecommerce/UserSide/commonwidgets/input_field.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

import 'login.dart';

class Registration extends StatefulWidget {
  @override
  _RegistrationState createState() => _RegistrationState();
}

class _RegistrationState extends State<Registration> with SingleTickerProviderStateMixin {
  final _auth = FirebaseAuth.instance;
  bool showSpinner;
  String email;
  String password;
  String name;

  CollectionReference _collectionReference = FirebaseFirestore.instance.collection('user-data');

  Future<void> sendUserData(){
    User currentUser = _auth.currentUser;

    return _collectionReference.doc(currentUser.email).set({
      'name':name,
      'email':email,
      'role':'user',
    }).then((value) => print('DataAdded')).catchError((error)=> print('Failed:$error'));
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Congratulations!!!'),
          content: SingleChildScrollView(
            child: ListBody(
              children: const <Widget>[
                Text('Your account has been created successfully.'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Log in'),
              onPressed: () {
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                        builder: (context) => Login()),
                        (route) => false);
              },
            ),
          ],
        );
      },
    );
  }

  @override
  void initState() {
    showSpinner = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueAccent[50],
      body: AnimatedBackground(
        vsync: this,
        behaviour: RacingLinesBehaviour(),
        child: Container(
          alignment: Alignment.center,
          child: ModalProgressHUD(
            inAsyncCall: showSpinner,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 24.0),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Flexible(
                      child: Hero(
                        tag: 'logo',
                        child: Icon(
                          Icons.shopping_cart,
                          size: 120,
                          color: Colors.pink,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Hero(
                      tag: 'HeroTitle',
                      child: Text(
                        'E-Shopping',
                        style: TextStyle(
                          decoration: TextDecoration.none,
                            color: Colors.pink,
                            fontFamily: 'Poppins',
                            fontSize: 26,
                            fontWeight: FontWeight.w700),
                      ),
                    ),
                    SizedBox(
                      height: 48.0,
                    ),
                    InputFiled(
                      hintText: 'Enter name',
                      leading: Icons.person,
                      color: Colors.blueAccent,
                      obscure: false,
                      userTyped: (value) {
                        name = value;
                      },
                    ),
                    InputFiled(
                      hintText: 'Enter e-mail',
                      leading: Icons.mail,
                      color: Colors.blueAccent,
                      keyboard: TextInputType.emailAddress,
                      obscure: false,
                      userTyped: (value) {
                        email = value;
                      },
                    ),
                    InputFiled(
                      hintText: 'Password',
                      leading: Icons.lock,
                      color: Colors.blueAccent,
                      keyboard: TextInputType.visiblePassword,
                      obscure: true,
                      userTyped: (value) {
                        password = value;
                      },
                    ),
                    SizedBox(
                      height: 24.0,
                    ),
                    CustomButton(
                      onpress: () async {
                        if (name != null && password != null && email != null) {
                          setState(() {
                            showSpinner = true;
                          });
                          try {
                            final newUser =
                                await _auth.createUserWithEmailAndPassword(
                              email: email,
                              password: password,
                            );
                            if (newUser != null) {
                              await newUser.user.updateDisplayName(name);
                              _showMyDialog();
                            }
                            sendUserData();
                          } catch (e) {
                            setState(() {
                              showSpinner = false;
                            });
                            EdgeAlert.show(context,
                                title: 'Signup Failed',
                                description: e.toString(),
                                gravity: EdgeAlert.BOTTOM,
                                icon: Icons.error,
                                backgroundColor: Colors.blueAccent);
                          }
                        } else {
                          EdgeAlert.show(context,
                              title: 'Signup Failed',
                              description: 'All fields are required.',
                              gravity: EdgeAlert.BOTTOM,
                              icon: Icons.error,
                              backgroundColor: Colors.blueAccent);
                        }
                      },
                      text: 'sign up',
                      accentColor: Colors.white,
                      mainColor: Colors.blueAccent,
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    GestureDetector(
                        onTap: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return Login();
                          }));
                        },
                        child: Text(
                          'or log in instead',
                          style: TextStyle(
                              fontFamily: 'Poppins',
                              fontSize: 14,
                              color: Colors.pink),
                        ))
                  ],
                ),

            ),
          ),
        ),
      ),
    );
  }
}
