import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ecommerce/UserSide/View/homePage.dart';

class ConfirmOrder extends StatefulWidget {

  @override
  _ConfirmOrderState createState() => _ConfirmOrderState();
}

class _ConfirmOrderState extends State<ConfirmOrder> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          color: Colors.pink[50],
          child: Column(
            children: [
              SizedBox(height: 30.0,),
              Image.asset('images/checkout/congo.gif', height:200.0),
              Expanded(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 30.0),
                        child: Text(
                          'Order Successfully',
                          style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Text(
                        'Placed!',
                        style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 45, right: 45, top: 20),
                        child: Text(
                          'Your order for all products has been successfully placed and has been processed for delivery.',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.pink),
                        ),
                      ),
                    ],
                  )),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Container(
        color: Colors.pink[50],
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: MaterialButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => HomePage()));
            },
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text("Continue Shopping",
                style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 20.0),
              ),
            ),
            color: Colors.pink,

          ),
        ),
      ),
    );
  }
}
