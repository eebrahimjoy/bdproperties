import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ecommerce/AdminPanel/admin_home_page.dart';
import 'package:flutter_ecommerce/UserSide/View/favourite.dart';

import 'cart/cart.dart';

class CustomDrawer extends StatelessWidget {
  String userName;
  String email;
  bool visibile;
  Function onPressed;
  CustomDrawer({this.userName,this.email,this.visibile,this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          UserAccountsDrawerHeader(
            decoration: BoxDecoration(
              color: Colors.pink,
            ),
            accountName: Text(userName),
            accountEmail: Text(email),
            currentAccountPicture: GestureDetector(
              child: CircleAvatar(
                backgroundColor: Colors.grey,
                child: Icon(Icons.person, color: Colors.white),
              ),
            ),
          ),

          InkWell(
            onTap: (){
              Navigator.pop(context);
            },
            child: ListTile(
              title: Text("Home Page"),
              leading: Icon(Icons.home, color: Colors.pink),
            ),
          ),

          InkWell(
            onTap: (){},
            child: ListTile(
              title: Text("My Account"),
              leading: Icon(Icons.person, color: Colors.pink),
            ),
          ),

          InkWell(
            onTap: (){},
            child: ListTile(
              title: Text("My Orders"),
              leading: Icon(Icons.shopping_basket, color: Colors.pink),
            ),
          ),

          InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context)=>Cart()));
            },
            child: ListTile(
              title: Text("Shopping cart"),
              leading: Icon(Icons.shopping_cart, color: Colors.pink),
            ),
          ),

          InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context)=>FavouritePage()));
            },
            child: ListTile(
              title: Text("Favourites"),
              leading: Icon(Icons.favorite, color: Colors.pink),
            ),
          ),

          Divider(),

          InkWell(
            onTap: (){},
            child: ListTile(
              title: Text("Settings"),
              leading: Icon(Icons.settings),
            ),
          ),

          InkWell(
            onTap: (){},
            child: ListTile(
              title: Text("About"),
              leading: Icon(Icons.help, color: Colors.blue),
            ),
          ),
          Visibility(
            visible: visibile,
            child:  InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context){
                return AdminHomePage();
              }));
            },
            child: ListTile(
              title: Text("Go to Admin Panel"),
              leading: Icon(Icons.admin_panel_settings, color: Colors.blue),
            ),
          ),),
          InkWell(
            onTap: onPressed,
            child: ListTile(
              title: Text("Log out"),
              leading: Icon(Icons.logout, color: Colors.blue),
            ),
          ),
        ],
      ),
    );
  }
}