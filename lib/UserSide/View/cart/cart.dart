import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ecommerce/UserSide/View/cart/provider.dart';
import 'package:flutter_ecommerce/UserSide/View/checkout/checkout.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

import 'DataBaseControl/cartProducts.dart';

class Cart extends StatefulWidget {

  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {

  Future<Stream<int>> numberOfProductsOnCart() async {
    return FirebaseFirestore.instance
        .collection('Cart-Products')
        .doc(FirebaseAuth.instance.currentUser.email)
        .collection('cart-list')
        .snapshots().map((documentSnapshot) => documentSnapshot.docs.length);
  }

  @override
  Widget build(BuildContext context) {

    final providerData = Provider.of<Total>(context);

    return Scaffold(
      appBar: AppBar(
        elevation: 0.2,
        backgroundColor: Colors.pink,
        title: Text("Your Cart")
      ),

      body: CartProducts(),

      bottomNavigationBar: Container(
        color: Colors.white,
        child: Row(
          children: [
            Expanded(
                child: ListTile(
                  title: Text("Total:"),
                  subtitle: Text("\$" + providerData.tot.toString()),
                )
            ),
            Expanded(
                child: MaterialButton(
                  onPressed: (){
                    // var num = numberOfProductsOnCart();
                    if(providerData.tot!=0){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>Checkout()));
                    }
                    else{
                      Fluttertoast.showToast(
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.CENTER,
                          backgroundColor: Colors.black,
                          textColor: Colors.white,
                          fontSize: 16.0,
                          msg: 'Your cart is empty.');
                    }
                  },
                  child: Text("Place Order",style: TextStyle(
                    color:Colors.white
                  ),),
                  color: Colors.pink,
                )
            )
          ],
        ),
      ),

    );
  }
}
