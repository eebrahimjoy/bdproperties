import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import '../singleCartProduct.dart';

class CartProducts extends StatefulWidget {

  @override
  _CartProductsState createState() => _CartProductsState();
}

class _CartProductsState extends State<CartProducts> {

  // var productsOnCart = [
  //   {
  //     "name":"Blazer",
  //     "picture":"images/products/blazer1.jpeg",
  //     "price":85,
  //     "size" :"M",
  //     "color":"Red",
  //     "quantity":1
  //   }
  // ];


  List productListOnCart = [];


  fetchProductsOnCart()async{
    FirebaseFirestore _fireStore = FirebaseFirestore.instance;

    QuerySnapshot snap =
    await _fireStore.collection('Cart-Products')
        .doc(FirebaseAuth.instance.currentUser.email)
        .collection('cart-list')
        .get();

    setState(() {
      for(int i=0;i<snap.docs.length;i++) {
        productListOnCart.add(
            {
              'product-name': snap.docs[i]['productName'],
              'product-Images': snap.docs[i]['images'],
              'product-Price': snap.docs[i]['price'],
              'product-Size': snap.docs[i]['size'],
              'product-Id':snap.docs[i].id
            }
        );
      }
    });
  }

  @override
  void initState() {
    super.initState();
    fetchProductsOnCart();
  }

  @override
  Widget build(BuildContext context) {

    // final providerData = Provider.of<Total>(context);

    return ListView.builder(
        itemCount: productListOnCart.length,
        itemBuilder: (context,index){
          return SingleCartProduct(
            cartProductName: productListOnCart[index]["product-name"],
            cartProductPicture: productListOnCart[index]["product-Images"],
            cartProductPrice: productListOnCart[index]["product-Price"],
            cartProductSize: productListOnCart[index]["product-Size"],
            cartProductId: productListOnCart[index]['product-Id'],
          );
        }
    );
  }
}
