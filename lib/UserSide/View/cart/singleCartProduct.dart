import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ecommerce/UserSide/View/cart/provider.dart';
import 'package:provider/provider.dart';

class SingleCartProduct extends StatefulWidget {
  final cartProductName;
  final cartProductPicture;
  final cartProductPrice;
  final cartProductSize;
  final cartProductColor;
  final cartProductId;
  final cartProductState;


  SingleCartProduct(
      {this.cartProductName,
      this.cartProductPicture,
      this.cartProductPrice,
      this.cartProductSize,
      this.cartProductColor,
      this.cartProductId,
        this.cartProductState
      });

  @override
  _SingleCartProductState createState() => _SingleCartProductState();
}

class _SingleCartProductState extends State<SingleCartProduct> {
  int cartProductQuantity = 0;



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final providerData = Provider.of<Total>(context);

    return Column(
      children: [
        Card(
          child: ListTile(
            leading: Image.network(
              widget.cartProductPicture,
              width: 80.0,
              height: 80.0,
            ),
            title: Text(widget.cartProductName),
            subtitle: Column(
              children: [
                Column(
                  children: [
                    Row(children: [
                      Padding(
                          padding: const EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
                          child: Text("Size:")),
                      Padding(
                          padding: const EdgeInsets.fromLTRB(5.0, 5.0, 0.0, 0.0),
                          child: Text(
                            widget.cartProductSize,
                            style: TextStyle(color: Colors.pink),
                          )),
                    ]),
                    Row(children: [
                      Padding(
                          padding: const EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
                          child: Text("Color:")),
                      Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Text(
                            // cartProductColor,
                            'Red',
                            style: TextStyle(color: Colors.pink),
                          )),
                    ]),
                  ],
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "\$${int.parse(widget.cartProductPrice) * cartProductQuantity}",
                    style: TextStyle(
                        color: Colors.pink,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold),
                  ),
                )
              ],
            ),
            trailing: Container(
              width: 120.0,
              child: Row(
                children: [
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(5.0),
                          topLeft: Radius.circular(5.0),
                        ),
                        color: Colors.pink),
                    child: Center(
                      child: IconButton(
                          onPressed: () {
                            print(widget.cartProductId);
                            setState(() {
                              if (cartProductQuantity >0) {
                                cartProductQuantity--;
                                providerData.Decrement(widget.cartProductPrice);
                              }
                            });
                            if (cartProductQuantity == 0) {
                              showDialog(
                                  context: context,
                                  builder: (context) {
                                    return AlertDialog(
                                      title: Text(
                                        'Delete',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.pink,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      content:
                                          Text('Do you want to delete this item?'),
                                      actions: [
                                        Row(
                                          children: [
                                            Expanded(
                                              child: MaterialButton(
                                                onPressed: () {
                                                  Navigator.of(context)
                                                      .pop(context);
                                                },
                                                child: Text(
                                                  'NO',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight: FontWeight.bold),
                                                ),
                                                color: Colors.pink,
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10.0,
                                            ),
                                            Expanded(
                                              child: MaterialButton(
                                                onPressed:()async{
                                                  await FirebaseFirestore.instance
                                                      .collection('Cart-Products')
                                                      .doc(FirebaseAuth.instance
                                                      .currentUser.email)
                                                      .collection('cart-list')
                                                      .doc(widget.cartProductId)
                                                      .delete();
                                                  setState(() {

                                                  });


                                                  Navigator.pop(context);
                                                },
                                               child: Text('YES',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight: FontWeight.bold),
                                                ),
                                                color: Colors.pink,
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    );
                                  });
                            }
                          },
                          icon: Icon(Icons.remove, color: Colors.white)),
                    ),
                    width: 40.0,
                  ),
                  Container(
                    width: 40.0,
                    color: Colors.pink.withOpacity(0.4),
                    child: Center(
                      child: Text(
                        "$cartProductQuantity",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18.0),
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(5.0),
                          topRight: Radius.circular(5.0),
                        ),
                        color: Colors.pink),
                    child: Center(
                      child: IconButton(
                          onPressed: () {
                            setState(() {
                              cartProductQuantity++;
                              print(widget.cartProductPrice);
                              providerData.Increment(widget.cartProductPrice);
                            });
                          },
                          icon: Icon(Icons.add, color: Colors.white)),
                    ),
                    width: 40.0,
                  ),
                ],
              ),
            ),
          ),
        ),

      ],
    );

  }
}
