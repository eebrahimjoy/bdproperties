import 'package:flutter/material.dart';

AppBar buildAppBar({Function onPressedSearch, Function onPressedCart}) {
  return AppBar(
    elevation: 0.2,
    backgroundColor: Colors.pink,
    title: Text("E-Shopping"),
    actions: [
      IconButton(
          onPressed: onPressedSearch,
          icon: Icon(
            Icons.search,
            color: Colors.white,
          )
      ),
      IconButton(
          onPressed: onPressedCart,
          icon: Icon(
            Icons.shopping_cart,
            color: Colors.white,
          )
      )
    ],
  );
}