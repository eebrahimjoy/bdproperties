import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ecommerce/UserSide/View/products/productDetails/productDetails.dart';

class SingleProduct extends StatelessWidget {

  final productName;
  final productPicture;
  final productOldPrice;
  final productPrice;
  final productDescription;
  final productSize;

  SingleProduct({
    this.productName,
    this.productPicture,
    this.productOldPrice,
    this.productPrice,
    this.productDescription,
    this.productSize,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Hero(
        tag: Text(productName),
        //child: Material(
          child: InkWell(
            onTap: () {
                Navigator.of(context).push(
                MaterialPageRoute(
                    builder: (context)=>ProductDetails(
                        productDetailName: productName,
                        productDetailImage: productPicture,
                        productDetailOldPrice: productOldPrice,
                        productDetailPrice: productPrice,
                        productDetailDescription: productDescription,
                        productDetailSize: productSize,
                    )
                )
            );},
            child: GridTile(
              footer: Container(
                color: Colors.white70,
                height: 40.0,
                child: Row(
                  children: [
                    Expanded(
                        child: Text(productName,
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
                        )),
                    Text("\$$productPrice",
                        style: TextStyle(color: Colors.pink, fontSize: 16.0)
                    )
                  ],
                ),
              ),
              child:Image.network(productPicture,
                  fit: BoxFit.cover
              ),
            ),
          ),
        //),
      ),
    );
  }
}