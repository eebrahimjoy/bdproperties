import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ecommerce/UserSide/View/products/singleProduct.dart';

class Products extends StatefulWidget {

  @override
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {


  List productList = [];

  /*var productList = [
    {
      "name":"Blazer",
      "picture":"images/products/blazer1.jpeg",
      "old_price":120,
      "price":85,
    },
    {
      "name":"Red dress",
      "picture":"images/products/dress1.jpeg",
      "old_price":100,
      "price":50,
    },
    {
      "name":"Black dress",
      "picture":"images/products/dress2.jpeg",
      "old_price":170,
      "price":100,
    },
    {
      "name":"Woman Blazer",
      "picture":"images/products/blazer2.jpeg",
      "old_price":100,
      "price":85,
    },
    {
      "name":"Heel",
      "picture":"images/products/heels1.jpeg",
      "old_price":200,
      "price":177,
    },
    {
      "name":"Heel",
      "picture":"images/products/heels2.jpeg",
      "old_price":120,
      "price":85,
    },
    {
      "name":"Pant",
      "picture":"images/products/pants1.jpg",
      "old_price":233,
      "price":210,
    },
    {
      "name":"Shoe",
      "picture":"images/products/shoe1.jpg",
      "old_price":175,
      "price":110,
    },
    {
      "name":"Pant",
      "picture":"images/products/pants2.jpeg",
      "old_price":120,
      "price":85,
    },
    {
      "name":"Skirt",
      "picture":"images/products/skt1.jpeg",
      "old_price":90,
      "price":85,
    },
    {
      "name":"Skirt",
      "picture":"images/products/skt2.jpeg",
      "old_price":120,
      "price":108,
    }
  ];*/



  fetchProducts()async{
    FirebaseFirestore _fireStore = FirebaseFirestore.instance;

    QuerySnapshot snap = await _fireStore.collection('Products').get();
    setState(() {
      for(int i=0;i<snap.docs.length;i++) {
        productList.add(
            {
              'product-name': snap.docs[i]['productName'],
              'product-Price': snap.docs[i]['price'],
              'product-Description': snap.docs[i]['description'],
              'product-Images': snap.docs[i]['images'],
              'old_price':'120',
              'product-Size': snap.docs[i]['size'],
            }
        );
      }
    });
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchProducts();
  }

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        itemCount: productList.length,
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemBuilder: (BuildContext context, int index){
          return SingleProduct(
            productName: productList[index]['product-name'],
            productPicture: productList[index]['product-Images'][0],
            productOldPrice: productList[index]['old_price'],
            productPrice: productList[index]['product-Price'],
            productDescription: productList[index]['product-Description'],
            productSize: productList[index]['product-Size'],
          );
        }
    );
  }
}


