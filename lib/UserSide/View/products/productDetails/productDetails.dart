import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ecommerce/UserSide/View/products/productDetails/productDetailsButton.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../cart/cart.dart';
import '../../customAppBar.dart';
import '../products.dart';

class ProductDetails extends StatefulWidget {
  final productDetailName;
  final productDetailImage;
  final productDetailOldPrice;
  final productDetailPrice;
  final productDetailDescription;
  final productDetailSize;

  ProductDetails(
      {@required this.productDetailName,
      @required this.productDetailImage,
      @required this.productDetailOldPrice,
      @required this.productDetailPrice,
      @required this.productDetailDescription,
      this.productDetailSize});

  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  var list1;

  var list2;



  String selectedSize = '';

  String selectedColor = '';

  // final record = Record.fromSnapshot(data);

  @override
  void initState() {
    super.initState();
    list1 = widget.productDetailSize;
    list2 = ['Black', 'Blue', 'Pink', 'Green', 'Yellow', 'White'];
  }

  String role;
  CollectionReference _collectionReference =
      FirebaseFirestore.instance.collection('Cart-Products');

  Future<void> addCartProduct() async {
    User user = FirebaseAuth.instance.currentUser;
    final DocumentSnapshot snap = await FirebaseFirestore.instance
        .collection('user-data')
        .doc(user.email)
        .get();
    setState(() {
      role = snap['role'];
    });
    if (role == 'user') {
      return _collectionReference
          .doc(user.email)
          .collection('cart-list')
          .add({
            'user-email': user.email,
            'productName': widget.productDetailName,
            'images': widget.productDetailImage,
            'price': widget.productDetailPrice,
            'size': selectedSize,
            'color': selectedColor,
          })
          .then((value) => print('Cart Product Added'))
          .catchError((error) => print('Failed:$error'));
    }
  }

  Future<void> addFavouriteProduct() async {
    User user = FirebaseAuth.instance.currentUser;
    CollectionReference _favcollectionReference =
        FirebaseFirestore.instance.collection('Favourite-Products');

    return _favcollectionReference
        .doc(user.email)
        .collection('item-list')
        .add({
          'productName': widget.productDetailName,
          'images': widget.productDetailImage,
          'price': widget.productDetailPrice,
          'size': selectedSize,
          'color': selectedColor,
        })
        .then((value) => print('Cart Product Added'))
        .catchError((error) => print('Failed:$error'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(onPressedSearch: () {
        //Navigator.push(context, MaterialPageRoute(builder: (context)=>Cart()));
      }, onPressedCart: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => Cart()));
      }),
      body: ListView(
        children: [
          Container(
            height: 300.0,
            child: GridTile(
              child: Container(
                color: Colors.white,
                child: Image.network(widget.productDetailImage),
              ),
              footer: Container(
                color: Colors.white70,
                child: ListTile(
                  leading: Text(widget.productDetailName,
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 16.0)),
                  title: Row(
                    children: [
                      Expanded(
                          child: Text(
                        "\$${widget.productDetailOldPrice}",
                        style: TextStyle(
                            color: Colors.grey,
                            decoration: TextDecoration.lineThrough),
                      )),
                      Expanded(
                          child: Text(
                        "\$${widget.productDetailPrice}",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.pink),
                      ))
                    ],
                  ),
                ),
              ),
            ),
          ),
          Row(
            children: [
              ProductDetailsButton(
                productDetail: Text("Size: $selectedSize"),
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                            insetPadding: EdgeInsets.symmetric(horizontal: 120),
                            actions: [
                              Center(
                                child: ButtonTheme(
                                  child: DropdownButton(
                                    underline: Container(
                                      height: 0.5,
                                      color: Colors.grey,
                                    ),
                                    hint: Text(
                                      ' Size',
                                      style: TextStyle(color: Colors.pink),
                                    ),
                                    isExpanded: false,
                                    onChanged: (value) {
                                      setState(() {
                                        selectedSize = value;
                                        Navigator.of(context).pop(context);
                                      });
                                    },
                                    items: list1
                                        .map<DropdownMenuItem<dynamic>>((value) {
                                      return DropdownMenuItem(
                                          value: value, child: Text(value));
                                    }).toList(),
                                  ),
                                ),
                              ),
                            ]);
                      });
                },
              ),
              ProductDetailsButton(
                productDetail: Text("Color: $selectedColor"),
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(

                            insetPadding: EdgeInsets.symmetric(horizontal: 120,),
                            actions: [
                              Center(
                                child: DropdownButton(
                                  underline: Container(
                                    height: 0.5,
                                    color: Colors.grey,
                                  ),
                                  hint: Text(
                                    ' Color',
                                    style: TextStyle(color: Colors.pink),
                                  ),
                                  isExpanded: false,
                                  onChanged: (value) {
                                    setState(() {
                                      selectedColor = value;
                                      Navigator.of(context).pop(context);
                                    });
                                  },
                                  items: list2
                                      .map<DropdownMenuItem<dynamic>>((value) {
                                    return DropdownMenuItem(
                                        value: value, child: Text(value));
                                  }).toList(),
                                ),
                              ),
                            ]);
                      });
                },
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                  child: MaterialButton(
                onPressed: () async {
                  User user = FirebaseAuth.instance.currentUser;
                  final DocumentSnapshot snap = await FirebaseFirestore.instance
                      .collection('user-data')
                      .doc(user.email)
                      .get();
                  setState(() {
                    role = snap['role'];
                  });
                  if (role == 'admin') {
                    Fluttertoast.showToast(
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.CENTER,
                        backgroundColor: Colors.black,
                        textColor: Colors.white,
                        fontSize: 16.0,
                        msg: 'List can be only added for users');
                  } else {
                    if (selectedSize == '' || selectedColor == '') {
                      Fluttertoast.showToast(
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.CENTER,
                          backgroundColor: Colors.black,
                          textColor: Colors.white,
                          fontSize: 16.0,
                          msg: 'Product not added to cart');
                    } else {
                      addCartProduct();
                      Fluttertoast.showToast(
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.CENTER,
                          backgroundColor: Colors.black,
                          textColor: Colors.white,
                          fontSize: 16.0,
                          msg: 'Product added to cart');
                    }
                  }
                },
                color: Colors.pink,
                textColor: Colors.white,
                elevation: 0.2,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Add to Cart"),
                    Icon(
                      Icons.add_shopping_cart,
                      color: Colors.white,
                    )
                  ],
                ),
              )),
              // IconButton(
              //     onPressed: () {},
              //     icon: Icon(Icons.add_shopping_cart, color: Colors.white)),
              StreamBuilder(
                  stream: FirebaseFirestore.instance
                      .collection('Favourite-Products')
                      .doc(FirebaseAuth.instance.currentUser.email)
                      .collection('item-list')
                      .where('productName', isEqualTo: widget.productDetailName)
                      .snapshots(),
                  builder: (context, snapshot) {
                    if(snapshot.data==null){
                      return Icon(Icons.favorite_border, color: Colors.pink);
                    }
                    return  IconButton(
                        onPressed: () {
                          snapshot.data.docs.length==0?addFavouriteProduct():Fluttertoast.showToast(
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              backgroundColor: Colors.black,
                              textColor: Colors.white,
                              fontSize: 16.0,
                              msg: 'Product already added to favorite');
                        },
                        icon: snapshot.data.docs.length==0? Icon(Icons.favorite_border, color: Colors.pink):
                    Icon(Icons.favorite)
                    );
                  }),
            ],
          ),
          Divider(),
          ListTile(
            title: Text("Product Details"),
            subtitle: Text(
              widget.productDetailDescription,
              textAlign: TextAlign.justify,
            ),
          ),
          Divider(),
          Row(
            children: [
              Padding(
                padding: EdgeInsets.fromLTRB(12.0, 5.0, 5.0, 5.0),
                child: Text(
                  "Product name",
                  style: TextStyle(color: Colors.grey),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(5.0),
                child: Text(widget.productDetailName),
              )
            ],
          ),
          Row(
            children: [
              Padding(
                padding: EdgeInsets.fromLTRB(12.0, 5.0, 5.0, 5.0),
                child: Text(
                  "Product brand",
                  style: TextStyle(color: Colors.grey),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(5.0),
                child: Text("Brand X"),
              )
            ],
          ),
          Row(
            children: [
              Padding(
                padding: EdgeInsets.fromLTRB(12.0, 5.0, 5.0, 5.0),
                child: Text(
                  "Product condition",
                  style: TextStyle(color: Colors.grey),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(5.0),
                child: Text("New"),
              )
            ],
          ),
          Divider(),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("Similar Products",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                  color: Colors.pink,
                ),
                textAlign: TextAlign.center),
          ),
          Container(
            height: 350.0,
            child: Products(),
          )
        ],
      ),
    );
  }
}
//
// class Record{
//   String email;
//   String productDetailName;
//   String productDetailImage;
//   String productDetailPrice;
//   String selectedSize;
//   String selectedColor;
//
//   Record(
//       this.email,
//       this.productDetailName,
//       this.productDetailImage,
//       this.productDetailPrice,
//       this.selectedSize,
//       this.selectedColor
//   );
//
//   Record.fromMap(Map<String, dynamic> map, {this.reference}):
//       assert(map['email']!=null),
//       assert(map['productDetailName']!=null),
//       assert(map['productDetailImage']!=null),
//       assert(map['productDetailPrice']!=null),
//       assert(map['selectedSize']!=null),
//       assert(map['selectedColor']!=null),
//
//       email = map['email'],
//       productDetailName = map['productDetailName'],
//       productDetailImage = map['productDetailImage'],
//       productDetailPrice = map['productDetailPrice'],
//       selectedSize = map['selectedSize'],
//       selectedColor = map['selectedColor'];
//
//     Map toMap() => {
//
//     };
// }
