import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProductDetailsButton extends StatelessWidget {

  final productDetail;
  final onPressed;

  ProductDetailsButton({
    this.productDetail,
    this.onPressed
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: MaterialButton(
          onPressed: onPressed,
          color: Colors.white,
          textColor: Colors.grey,
          elevation: 0.2,
          child: Row(
            children: [
              Expanded(
                child: productDetail,
              ),
              Expanded(
                child: Icon(Icons.arrow_drop_down),
              )
            ],
          ),
        )
    );
  }
}