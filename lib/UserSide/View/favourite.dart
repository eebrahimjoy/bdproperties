import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class FavouritePage extends StatefulWidget {
  const FavouritePage({Key key}) : super(key: key);

  @override
  _FavouritePageState createState() => _FavouritePageState();
}

class _FavouritePageState extends State<FavouritePage> {

  List favList = [];

  fetchProductsOnFav() async {
    FirebaseFirestore _fireStore = FirebaseFirestore.instance;

    QuerySnapshot snap = await _fireStore
        .collection('Favourite-Products')
        .doc(FirebaseAuth.instance.currentUser.email)
        .collection('item-list')
        .get();

    setState(() {
      for (int i = 0; i < snap.docs.length; i++) {
        favList.add({
          'product-name': snap.docs[i]['productName'],
          'product-Images': snap.docs[i]['images'],
          'product-Price': snap.docs[i]['price'],
          'product-Id': snap.docs[i].id
        });
      }
    });
  }

  deleteFavItem(String docId) async {
    await FirebaseFirestore.instance
        .collection('Favourite-Products')
        .doc(FirebaseAuth.instance.currentUser.email)
        .collection('item-list')
        .doc(docId)
        .delete();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchProductsOnFav();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          elevation: 0.2,
          backgroundColor: Colors.pink,
          title: Text("Your Favourites")),
      body: ListView.builder(
          itemCount: favList.length,
          itemBuilder: (context, index) {
        return Card(
          child: ListTile(
            leading: Image.network(
              favList[index]['product-Images'],
              width: 80.0,
              height: 80.0,
            ),
            title: Text(favList[index]['product-name']),
            trailing: Container(
              width: 60.0,
              child: Row(
                children: [
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(5.0),
                          topLeft: Radius.circular(5.0),
                        ),
                        color: Colors.pink),
                    child: Center(
                      child: IconButton(
                          onPressed: () {
                            deleteFavItem(favList[index]['product-Id']);
                            fetchProductsOnFav();
                            setState(() {});

                          },
                          icon:
                              Icon(Icons.cancel_outlined, color: Colors.white)),
                    ),
                    width: 60.0,
                  ),
                ],
              ),
            ),
          ),
        );
      }),
    );
  }
}
