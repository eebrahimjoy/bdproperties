import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ImageCarousel extends StatelessWidget {
  List<String> carouselImages = [];
  ImageCarousel({this.carouselImages});
  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
      options: CarouselOptions(
          autoPlay: true,
          enlargeCenterPage: true,
          viewportFraction: 0.8,
          enlargeStrategy: CenterPageEnlargeStrategy.height,
          height: 200.0),
      items: carouselImages.map((i) {
        return Padding(
            padding: EdgeInsets.only(left: 3,right: 3),
           child: Container(
            decoration: BoxDecoration(
            image: DecorationImage(
              image: NetworkImage(i),
              fit: BoxFit.fitWidth
            )
        ),
        ),
        );
      }).toList(),
    );
  }
}

