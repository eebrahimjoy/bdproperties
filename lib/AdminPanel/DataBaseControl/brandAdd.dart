import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class AddBrand extends StatelessWidget {

  TextEditingController _brandController = TextEditingController();
  CollectionReference _collectionReference= FirebaseFirestore.instance.collection('Brands');




  Future<void> addBrand() {
    return _collectionReference.doc(_brandController.text).set(
        {'brandName': _brandController.text,
        }
    ).then((value) => print('CategoryAdded')).catchError((error)=> print('Failed:$error'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.all( 16.0),
            decoration: BoxDecoration(
              color: Colors.grey.shade200,
            ),
            padding: EdgeInsets.only(left: 10),
            width: MediaQuery.of(context).size.width * 0.90,
            child: TextFormField(
              controller: _brandController,
              autofocus: false,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'add brand name',
                hintStyle: TextStyle(
                  fontFamily: 'Poppins',
                ),
              ),
            ),
          ),
          SizedBox(height: 15.0,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextButton(onPressed: (){
                if(_brandController.text != ''){
                  addBrand();
                  Fluttertoast.showToast(
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.CENTER,
                      backgroundColor: Colors.black,
                      textColor: Colors.white,
                      fontSize: 16.0,
                      msg: 'Brand Added');
                  Navigator.pop(context);
                }else{
                  Fluttertoast.showToast(msg: 'Enter Brand name');
                }
              }, child: Text('ADD')),
              SizedBox(width: 30.0,),
              TextButton(onPressed: (){
                Navigator.pop(context);
              }, child: Text('CANCLE')),
            ],
          ),
        ],
      ),
    );
  }
}


