import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';

class AddCarouselImage extends StatefulWidget {
  const AddCarouselImage({Key key}) : super(key: key);

  @override
  _AddCarouselImageState createState() => _AddCarouselImageState();
}

class _AddCarouselImageState extends State<AddCarouselImage> {

  bool isLoading;
  File _fileImage;

  Widget _displayImage() {
    if (_fileImage == null) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(14, 70, 14, 70),
        child: new Icon(
          Icons.add,
          color: Colors.grey,
        ),
      );
    } else {
      return Image.file(_fileImage, fit: BoxFit.fill, width: double.infinity);
    }
  }

  Future<void> _selectImage() async {
    try {
      final image = await ImagePicker().pickImage(source: ImageSource.gallery);
      // final image2 = await ImagePicker().pickImage(source: ImageSource.gallery);

      setState(() {
        _fileImage = File(image.path);
      });
    } on PlatformException catch (e) {
      print('Failed to pick an Image:$e');
    }
  }

  Future<void> uploadImage() async {
    String imageUrl;
    FirebaseStorage _fireStorage = FirebaseStorage.instance;
    if (_fileImage != null) {
      setState(() {
        isLoading=true;
      });
      final String imagePath =
          "carouselimg${DateTime.now().millisecondsSinceEpoch.toString()}.jpg";
      TaskSnapshot uploadtask = await _fireStorage
          .ref()
          .child('CarouselImage')
          .child(imagePath)
          .putFile(_fileImage);
      imageUrl = await uploadtask.ref.getDownloadURL();

      addCarouselImage(imageUrl);
      setState(() {
        isLoading=false;
      });

      Fluttertoast.showToast(
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.black,
          textColor: Colors.white,
          fontSize: 16.0,
          msg: 'Image  Added');

      Navigator.pop(context);
    }else{
      Fluttertoast.showToast(
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.black,
          textColor: Colors.white,
          fontSize: 16.0,
          msg: 'Image not Added');

    }

  }

  Future<void> addCarouselImage(String image){
    FirebaseFirestore _fireStore = FirebaseFirestore.instance;
    CollectionReference _collection =_fireStore.collection('Carousel-Image');
    return _collection.doc().set({
      'caresoul-image':image,
    })

        .then((value) => print('ProductAdded')).catchError((error)=> print('Failed:$error'));

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:isLoading!=true? SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                          side: BorderSide(
                              width: 2.5, color: Colors.grey.withOpacity(0.5)),
                        ),
                        onPressed: () {
                          _selectImage();
                        },
                        child: _displayImage()),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 15.0,
            ),
            TextButton(
              style: TextButton.styleFrom(
                backgroundColor: Colors.red,
                padding: EdgeInsets.all(16.0),
              ),
              child: Text(
                'Add Image',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                uploadImage();
              },
            )
          ],
        ),
      ):Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.blueAccent),
        ),
      ),
    );
  }
}
