
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';

class AddProduct extends StatefulWidget {
  const AddProduct({Key key}) : super(key: key);

  @override
  _AddProductState createState() => _AddProductState();
}

class _AddProductState extends State<AddProduct> {
  Color white = Colors.white;
  MaterialColor grey = Colors.grey;
  Color black = Colors.black;
  bool isLoading;

  TextEditingController _productNameController = TextEditingController();
  TextEditingController _quantityController = TextEditingController();
  TextEditingController _priceController = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();
  String selectedCategory ;
  String  selectedBrand ;
  String valueChose ;
  List<String> selectedSizes = <String>[];
  File _fileImage1;
  File _fileImage2;
  File _fileImage3;


  CollectionReference _collectionReference= FirebaseFirestore.instance.collection('Products');

  Future<void> addProduct(List images) {
    return _collectionReference.doc().set(
        {'productName': _productNameController.text,
          'description': _descriptionController.text,
          'category':selectedCategory,
          'brand':selectedBrand,
          'quantity':_quantityController.text,
          'size':selectedSizes,

           'images':images,
          'price':_priceController.text
        }
    ).then((value) => print('ProductAdded')).catchError((error)=> print('Failed:$error'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.2,
        backgroundColor: white,
        leading: IconButton(
          icon:Icon(Icons.close),
          color: black,
          onPressed: (){
            Navigator.of(context).pop(context);
          },
        ),
        title: Text(
          "Add Product",
          style: TextStyle(color: black),
        ),
      ),
      body: SingleChildScrollView(
        child: isLoading !=true? Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                          shape: StadiumBorder(),
                          side: BorderSide(
                              width: 2.5, color: grey.withOpacity(0.5)),
                        ),
                        onPressed: () {
                          _selectImage( 1);
                        },
                        child: _displayChild1(),
                  ),
                ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                          shape: StadiumBorder(),
                          side: BorderSide(
                              width: 2.5, color: grey.withOpacity(0.5)),
                        ),
                        onPressed: () {
                          _selectImage( 2);
                        },
                        child: _displayChild2()),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                          shape: StadiumBorder(),
                          side: BorderSide(
                              width: 2.5, color: grey.withOpacity(0.5)),
                        ),
                        onPressed: () {
                          _selectImage( 3);
                        },
                        child: _displayChild3()),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'enter a product name with 10 characters at maximum',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.pink, fontSize: 12),
              ),
            ),

            /************Product Name*********/
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: TextFormField(
                controller: _productNameController,
                decoration: InputDecoration(hintText: 'Product name'),
                validator: (value) {
                  String err;
                  if (value.isEmpty) {
                    err = 'You must enter the product name';
                  } else if (value.length > 10) {
                    err = 'Product name cant have more than 10 letters';
                  }
                  return err;
                },
              ),
            ),
            /************Product Description*********/
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: TextFormField(
                controller: _descriptionController,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  hintText: 'Description',
                ),
                validator: (value){
                  String err;
                  if(value.isEmpty){
                    err= 'You must enter the product name';
                  }else{
                    err='All Okey';
                  }
                  return err;
                },
              ),
            ),

              Row(

                children: [
                  //---------------Category DropDown-------------//
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text('Category: ', style: TextStyle(color: Colors.pink),),
                  ),
                  StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestore.instance.collection('Categories').snapshots(),
                    builder: (BuildContext context,AsyncSnapshot<QuerySnapshot> snapshot){
                      if(!snapshot.hasData){
                        return Text('No data');
                      }
                      /*else{
            List<DropdownMenuItem> categoryItems =[];
            for(int i =0;i<snapshot.data.docs.length;i++){
              DocumentSnapshot snap = snapshot.data.docs[i];
            }
          }*/
                      return DropdownButton(
                        value: selectedCategory,
                        icon: const Icon(Icons.arrow_downward),
                        iconSize: 24,
                        elevation: 16,
                        style: const TextStyle(
                            color: Colors.deepPurple
                        ),
                        underline: Container(
                          height: 2,
                          color: Colors.deepPurpleAccent,
                        ),
                        onChanged: ( newValue) {
                          setState(() {
                            selectedCategory = newValue;
                          });
                        },
                        items:snapshot.data.docs.map((document){
                          return DropdownMenuItem(
                              value: document['categoryName'],
                              child: Text(document['categoryName']));
                        } ).toList(),
                      );

                    },
                  ),
                  //----------------Brand Dropdown-------------//
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text('Brand: ', style: TextStyle(color: Colors.pink),),
                  ),
                  StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestore.instance.collection('Brands').snapshots(),
                    builder: (BuildContext context,AsyncSnapshot<QuerySnapshot> snapshot){
                      if(!snapshot.hasData){
                        return Text('No data');
                      }
                      /*else{
            List<DropdownMenuItem> categoryItems =[];
            for(int i =0;i<snapshot.data.docs.length;i++){
              DocumentSnapshot snap = snapshot.data.docs[i];
            }
          }*/
                      return DropdownButton(
                        value: selectedBrand,
                        icon: const Icon(Icons.arrow_downward),
                        iconSize: 24,
                        elevation: 16,
                        style: const TextStyle(
                            color: Colors.deepPurple
                        ),
                        underline: Container(
                          height: 2,
                          color: Colors.deepPurpleAccent,
                        ),
                        onChanged: ( newValue) {
                          setState(() {
                            selectedBrand = newValue;
                          });
                        },
                        items:snapshot.data.docs.map((document){
                          return DropdownMenuItem(
                              value: document['brandName'],
                              child: Text(document['brandName']));
                        } ).toList(),
                      );

                    },
                  ),
                ],
              ),
            //-------------Quantity Field---------------//
            // Padding(
            //   padding: const EdgeInsets.all(12.0),
            //   child: TextFormField(
            //     controller: _quantityController,
            //     keyboardType: TextInputType.number,
            //     decoration: InputDecoration(
            //       hintText: 'Quantity',
            //     ),
            //     validator: (value){
            //       String err;
            //       if(value.isEmpty){
            //         err= 'You must enter the product name';
            //       }else{
            //         err='All Okey';
            //       }
            //       return err;
            //     },
            //   ),
            // ),



            SizedBox(height: 10.0,),
            Text('Available Sizes', style: TextStyle(color: Colors.pink, fontWeight: FontWeight.bold, decoration: TextDecoration.underline),),


               Row(
                 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [


                  Checkbox(value: selectedSizes.contains('S'), onChanged: (value) => changeSelectedSize('S')),
                  Text('S'),

                  Checkbox(value: selectedSizes.contains('M'), onChanged: (value) => changeSelectedSize('M')),
                  Text('M'),

                  Checkbox(value: selectedSizes.contains('L'), onChanged: (value) => changeSelectedSize('L')),
                  Text('L'),

                  Checkbox(value: selectedSizes.contains('XL'), onChanged: (value) => changeSelectedSize('XL')),
                  Text('XL'),

                  Checkbox(value: selectedSizes.contains('XXL'), onChanged: (value) => changeSelectedSize('XXL')),
                  Text('XXL'),
                ],
              ),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Checkbox(value: selectedSizes.contains('28'), onChanged: (value) => changeSelectedSize('28')),
                Text('28'),

                Checkbox(value: selectedSizes.contains('30'), onChanged: (value) => changeSelectedSize('30')),
                Text('30'),

                Checkbox(value: selectedSizes.contains('32'), onChanged: (value) => changeSelectedSize('32')),
                Text('32'),

                Checkbox(value: selectedSizes.contains('34'), onChanged: (value) => changeSelectedSize('34')),
                Text('34'),


                Checkbox(value: selectedSizes.contains('36'), onChanged: (value) => changeSelectedSize('36')),
                Text('36'),


              ],
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Checkbox(value: selectedSizes.contains('38'), onChanged: (value) => changeSelectedSize('38')),
                Text('38'),
                Checkbox(value: selectedSizes.contains('40'), onChanged: (value) => changeSelectedSize('40')),
                Text('40'),

                Checkbox(value: selectedSizes.contains('42'), onChanged: (value) => changeSelectedSize('42')),
                Text('42'),

                Checkbox(value: selectedSizes.contains('44'), onChanged: (value) => changeSelectedSize('44')),
                Text('44'),

                Checkbox(value: selectedSizes.contains('46'), onChanged: (value) => changeSelectedSize('46')),
                Text('46'),


              ],
            ),
            Row(
              children: [
                Checkbox(value: selectedSizes.contains('No Size'), onChanged: (value) => changeSelectedSize('No Size')),
                Text('No Size'),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: TextFormField(
                controller: _priceController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  hintText: 'Price',
                ),
                validator: (value){
                  String err;
                  if(value.isEmpty){
                    err= 'You must enter the Price';
                  }else{
                    err='All Okey';
                  }
                  return err;
                },
              ),
            ),

            TextButton(
              style:TextButton.styleFrom(
                backgroundColor: Colors.pink,
               padding: EdgeInsets.all(16.0),
              ),
              child: Text('Add product',style: TextStyle(color: Colors.white),),
              onPressed: (){
                setState(() {
                  isLoading = true;
                });

                // validateAndUpload();
                uploadProduct();

              },
            )

          ],
        ):Center(
          child: CircularProgressIndicator(
            valueColor:  AlwaysStoppedAnimation<Color>(Colors.blueAccent),
          ),
        ),
      ),
    );
  }



  Future<void> uploadProduct()async {


    String imageUrl1;
    String imageUrl2;
    String imageUrl3;
    FirebaseStorage  _fireStorage= FirebaseStorage.instance;
    if(_fileImage1!=null || _fileImage2!=null || _fileImage3!=null){
      if(_fileImage1!=null) {
        final String image1 = "img1${DateTime
            .now()
            .millisecondsSinceEpoch
            .toString()}.jpg";
        TaskSnapshot task1 = await _fireStorage.ref()
            .child('ProduuctPhotos')
            .child(image1)
            .putFile(_fileImage1);
        imageUrl1 = await task1.ref.getDownloadURL();

      }else if(_fileImage2!=null) {
        final String image2 = "img2${DateTime
            .now()
            .millisecondsSinceEpoch
            .toString()}.jpg";
        TaskSnapshot task2 = await _fireStorage.ref()
            .child('ProduuctPhotos')
            .child(image2)
            .putFile(_fileImage2);
        imageUrl2 = await task2.ref.getDownloadURL();

      }else if(_fileImage3!=null){
      final String image3 = "img3${DateTime.now().millisecondsSinceEpoch.toString()}.jpg";
      TaskSnapshot task3 =await _fireStorage.ref().child('ProduuctPhotos').child(image3).putFile(_fileImage3);

      imageUrl3 = await task3.ref.getDownloadURL();
      }
      List<String> imageList = [imageUrl1,imageUrl2,imageUrl3];

      addProduct(imageList);
      setState(() {
        isLoading = false;
      });

      Fluttertoast.showToast(
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.black,
          textColor: Colors.white,
          fontSize: 16.0,
          msg: 'Product Added');
      Navigator.pop(context);
    }else{
      setState(() {
        isLoading = false;
      });
      Fluttertoast.showToast(
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.black,
          textColor: Colors.white,
          fontSize: 16.0,
          msg: 'Add a product pic please');
    }
  }



  void changeSelectedSize(String size) {
    if(selectedSizes.contains(size)){
      setState(() {
        selectedSizes.remove(size);
      });
    }else{
      setState(() {
        selectedSizes.add( size);
      });
    }
  }


  Widget _displayChild1() {
    if(_fileImage1 == null){
      return Padding(
        padding: const EdgeInsets.fromLTRB(14, 70, 14, 70),
        child: new Icon(Icons.add, color: grey,),
      );
    }else{
      return Image.file(_fileImage1,fit: BoxFit.fill, width: double.infinity);
    }
  }

  Widget _displayChild2() {
    if(_fileImage2 == null){
      return Padding(
        padding: const EdgeInsets.fromLTRB(14, 70, 14, 70),
        child: new Icon(Icons.add, color: grey,),
      );
    }else{
      return Image.file(_fileImage2,fit: BoxFit.fill, width: double.infinity);
    }
  }
  Widget _displayChild3() {
    if(_fileImage3 == null){
      return Padding(
        padding: const EdgeInsets.fromLTRB(14, 70, 14, 70),
        child: new Icon(Icons.add, color: grey,),
      );
    }else{
      return Image.file(_fileImage3,fit: BoxFit.fill, width: double.infinity);
    }
  }



  Future _selectImage(int imageNumber) async{
    try {
      final image = await ImagePicker().pickImage(source: ImageSource.gallery);
      final tempImage = File(image.path);
      switch (imageNumber) {
        case 1:
          setState(() {
            _fileImage1 = tempImage;
          });
          break;
        case 2:
          setState(() {
            _fileImage2 = tempImage;
          });
          break;
        case 3:
          setState(() {
            _fileImage3 = tempImage;
          });
      }
    } on PlatformException catch(e){
      print('Failed to pick an Image:$e');
    }
  }


}








































//---------------------------extra
/*DropdownButton(
              value: valueChose,
              icon: const Icon(Icons.arrow_downward),
              iconSize: 24,
              elevation: 16,
              style: const TextStyle(
                  color: Colors.deepPurple
              ),
              underline: Container(
                height: 2,
                color: Colors.deepPurpleAccent,
              ),
              onChanged: ( newValue) {
                setState(() {
                  valueChose = newValue;
                });
              },
              items: list
                  .map(( value) {
                return DropdownMenuItem(
                  value: value,
                  child:  Text(value),
                );
              })
                  .toList(),
            ),*/
