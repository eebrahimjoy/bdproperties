import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_ecommerce/AdminPanel/DataBaseControl/brandAdd.dart';
import 'package:flutter_ecommerce/AdminPanel/DataBaseControl/categoryAdd.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ecommerce/AdminPanel/Productlist_page.dart';
import 'package:flutter_ecommerce/AdminPanel/brandList_page.dart';
import 'package:flutter_ecommerce/AdminPanel/categoryList_page.dart';

import 'DataBaseControl/Product_Add.dart';
import 'DataBaseControl/carousel_image_add.dart';


class CustomDrawerAdmin extends StatelessWidget {
  String adminName;
  String adminEmail;
  Function onPressed;
  CustomDrawerAdmin({this.adminName,this.adminEmail,this.onPressed});

  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  _signOut() async{
    await _firebaseAuth.signOut();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          UserAccountsDrawerHeader(
            decoration: BoxDecoration(
              color: Colors.pink,
            ),
            accountName: Text(adminName),
            accountEmail: Text(adminEmail),
            currentAccountPicture: GestureDetector(
              child: CircleAvatar(
                backgroundColor: Colors.grey,
                child: Icon(Icons.person, color: Colors.white),
              ),
            ),
          ),

          InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context){
                return AddProduct();
              }));
            },
            child: ListTile(
              title: Text("Add Product"),
              leading: Icon(Icons.add_circle, color: Colors.pink),
            ),
          ),

          InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context){
                return ShowProductItem();
              }));
            },
            child: ListTile(
              title: Text("Product list"),
              leading: Icon(Icons.list, color: Colors.pink),
            ),
          ),
          InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context){
                return AddCarouselImage();
              }));
            },
            child: ListTile(
              title: Text("Add Image"),
              leading: Icon(Icons.add_circle, color: Colors.pink),
            ),
          ),

          InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context){
                return AddCategory();
              }));
            },
            child: ListTile(
              title: Text("Add Category"),
              leading: Icon(Icons.category, color: Colors.pink),
            ),
          ),

          InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context){
                return ShowCategoryItem();
              }));
            },
            child: ListTile(
              title: Text("Category list"),
              leading: Icon(Icons.list, color: Colors.pink),
            ),
          ),

          InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context){
                return AddBrand();
              }));
            },
            child: ListTile(
              title: Text("Add Brand"),
              leading: Icon(Icons.branding_watermark, color: Colors.pink),
            ),
          ),


          InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context){
                return ShowBrandItem();
              }));
            },
            child: ListTile(
              title: Text("Brand list"),
              leading: Icon(Icons.list,color: Colors.pink),
            ),
          ),
      Divider(),
          InkWell(
            onTap: onPressed,
            child: ListTile(
              title: Text("Log out"),
              leading: Icon(Icons.logout, color: Colors.blue),
            ),
          )
        ],
      ),
    );
  }


  // List carouselImages = [];


  // fetchCarouselImages()async{
  //   FirebaseFirestore _fireStore = FirebaseFirestore.instance;
  //   QuerySnapshot snap = await _fireStore.collection('Carousel-Image').get();
  //   setState(() {
  //     for(int i=0;i<snap.docs.length;i++){
  //       carouselImages.add(snap.docs[i]['caresoul-image']);
  //     }
  //   });
  //
  // }
  //
  // @override
  // void initState() {
  //   // TODO: implement initState
  //   super.initState();
  //   // fetchCarouselImages();
  // }
}