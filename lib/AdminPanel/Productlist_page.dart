import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ShowProductItem extends StatefulWidget {
  const ShowProductItem({Key key}) : super(key: key);

  @override
  _ShowProductItemState createState() => _ShowProductItemState();
}

class _ShowProductItemState extends State<ShowProductItem> {

  List _products = [];
  List size =[];


  fetchProducts()async{
    FirebaseFirestore _fireStore = FirebaseFirestore.instance;

    QuerySnapshot snap = await _fireStore.collection('Products').get();
    setState(() {
      for(int i=0;i<snap.docs.length;i++) {
        size.add(snap.docs[i]['size'][0]);

        _products.add(
            {
              'product-name': snap.docs[i]['productName'],
              'product-Price': snap.docs[i]['price'],
              'product-Description': snap.docs[i]['description'],
              'product-Quantity': snap.docs[i]['quantity'],
              'product-Images': snap.docs[i]['images'],
              'product-Size':snap.docs[i]['size']
            }
        );
      }
    });
  }

  @override
  void initState() {
    super.initState();
    fetchProducts();
  }





  String selectedCategory;
  String selectedSize;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
              ElevatedButton(onPressed: (){
                print(_products);
                print(size);
              }, child: Text('Print Products')),
            SizedBox(height: 15.0,),
            Expanded(
              flex: 2,
              child: GridView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: _products.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 1),

                  itemBuilder: (context,index){
                return Card(
                  margin: EdgeInsets.symmetric(vertical: 10.0),
                  elevation: 3.0,
                  child: Container(
                    height: 40,
                    child: Column(
                      children: [
                        Image.network(
                          _products[index]['product-Images'][0],
                          height: 150,
                          fit: BoxFit.fill,),
                        Text(_products[index]['product-name']),
                        SizedBox(height: 15.0,),
                        Text(_products[index]['product-Price']),

                      ],
                    ),
                  ),
                );

                  }),
            ),
           /* StreamBuilder<QuerySnapshot>(
              stream: FirebaseFirestore.instance.collection('Products').snapshots(),
              builder: (BuildContext context,AsyncSnapshot<QuerySnapshot> snapshot){
                if(!snapshot.hasData){
                  return Text('No data');
                }
                /*else{
            List<DropdownMenuItem> categoryItems =[];
            for(int i =0;i<snapshot.data.docs.length;i++){
              DocumentSnapshot snap = snapshot.data.docs[i];
            }
          }*/
                return DropdownButton(
                  value: selectedSize,
                  icon: const Icon(Icons.arrow_downward),
                  iconSize: 24,
                  elevation: 16,
                  style: const TextStyle(
                      color: Colors.deepPurple
                  ),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: ( newValue) {
                    setState(() {
                      selectedSize = newValue;
                    });
                  },
                  items:snapshot.data.docs.map((document){
                    return DropdownMenuItem(
                        value: document['size'][0],
                        child: Text(document['size'][0]));
                  } ).toList(),
                );

              },
            ),
        DropdownButton<String>(
          value: selectedSize,
          icon: const Icon(Icons.arrow_downward),
          iconSize: 24,
          elevation: 16,
          style: const TextStyle(
              color: Colors.deepPurple
          ),
          underline: Container(
            height: 2,
            color: Colors.deepPurpleAccent,
          ),
          onChanged: ( newValue) {
            setState(() {
              selectedSize = newValue;
            });
          },
          items:size.map<DropdownMenuItem<String>>((document){
            return new DropdownMenuItem<String>(
                value: document,
                child: Text(document));
          } ).toList(),
        ),*/
            Expanded(
                flex: 2,
                child: Text('ex'))
          ],
        ),
      ),
    );
  }
}




/* Scaffold(
      body: StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestore.instance.collection('Categories').snapshots(),
        builder: (BuildContext context,AsyncSnapshot<QuerySnapshot> snapshot){
          if(!snapshot.hasData){
            return Text('No data');
          }
          /*else{
            List<DropdownMenuItem> categoryItems =[];
            for(int i =0;i<snapshot.data.docs.length;i++){
              DocumentSnapshot snap = snapshot.data.docs[i];
            }
          }*/
          return DropdownButton(
            value: selectedCategory,
            icon: const Icon(Icons.arrow_downward),
            iconSize: 24,
            elevation: 16,
            style: const TextStyle(
                color: Colors.deepPurple
            ),
            underline: Container(
              height: 2,
              color: Colors.deepPurpleAccent,
            ),
            onChanged: ( newValue) {
              setState(() {
                selectedCategory = newValue;
              });
            },
            items:snapshot.data.docs.map((document){
              return DropdownMenuItem(
                  value: document['categoryName'],
                  child: Text(document['categoryName']));
            } ).toList(),
          );

        },
      ),
    );*/
