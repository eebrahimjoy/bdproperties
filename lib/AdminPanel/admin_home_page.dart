import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ecommerce/AdminPanel/custom_Drawer_Admin.dart';
import 'package:flutter_ecommerce/UserSide/View/homePage.dart';
import 'package:flutter_ecommerce/UserSide/View/login.dart';

class AdminHomePage extends StatefulWidget {
  String adminName;
  String adminEmail;
  AdminHomePage({this.adminName,this.adminEmail});

  @override
  _AdminHomePageState createState() => _AdminHomePageState();
}

class _AdminHomePageState extends State<AdminHomePage> {
  String adminName;
  String adminEmail;
  String role;
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  bool isLoading = true;

  Future<void> checkAdmin() async {
    User user = _firebaseAuth.currentUser;
    final DocumentSnapshot snap = await FirebaseFirestore.instance
        .collection('user-data')
        .doc(user.email)
        .get();
    setState(() {
      role = snap['role'];
    });
    if (role == 'admin') {
      setState(() {
        adminName = snap['name'];
        adminEmail = user.email;
        isLoading = false;
      });
    }
  }

  Future<void> _signOut() async{
    await _firebaseAuth.signOut();
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => Login()),(route)=>false);
  }

  MaterialColor active = Colors.pink;
  MaterialColor notActive = Colors.grey;

  String category;
  String totalUser;
  String totalProduct;

  void getTotalCategory() async {
    FirebaseFirestore _firestore = FirebaseFirestore.instance;
    QuerySnapshot categorysnap =
        await _firestore.collection('Categories').get();
    setState(() {
      category = categorysnap.docs.length.toString();
    });
  }

  void getTotallUser() async {
    FirebaseFirestore _firestore = FirebaseFirestore.instance;
    QuerySnapshot userdatasnap = await _firestore.collection('user-data').get();
    setState(() {
      totalUser = userdatasnap.docs.length.toString();
    });
  }

  void getTotalProduct() async {
    FirebaseFirestore _firestore = FirebaseFirestore.instance;
    QuerySnapshot productdatasnap =
        await _firestore.collection('Products').get();
    setState(() {
      totalProduct = productdatasnap.docs.length.toString();
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getTotalCategory();
    getTotallUser();
    getTotalProduct();
    checkAdmin();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Dashboard'),
          centerTitle: true,
        ),
        drawer: CustomDrawerAdmin(
          adminName: adminName,
          adminEmail: adminEmail,
          onPressed: (){
            _signOut();
          },
        ),
        body: isLoading != true
            ? Column(
                children: [
                  ListTile(
                    subtitle: TextButton.icon(
                      onPressed: null,
                      icon: Icon(
                        Icons.attach_money,
                        size: 30.0,
                        color: Colors.green,
                      ),
                      label: Text('12,000',
                          textAlign: TextAlign.center,
                          style:
                              TextStyle(fontSize: 30.0, color: Colors.green)),
                    ),
                    title: Text(
                      'Revenue',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 24.0, color: Colors.grey),
                    ),
                  ),
                  TextButton(
                      style: TextButton.styleFrom(
                        backgroundColor: Colors.pink,
                        padding: EdgeInsets.all(16.0),
                      ),
                      child: Text(
                        'Go TO UserPanel',
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return HomePage(
                              userName: adminName, userEmail: adminEmail);
                        }));
                      }),
                  Expanded(
                    child: GridView(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2),
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(11.0),
                          child: Card(
                            child: ListTile(
                                title: TextButton.icon(
                                    onPressed: () {
                                      getTotallUser();
                                    },
                                    icon: Icon(Icons.people_outline),
                                    label: Text("Users")),
                                subtitle: Text(
                                  totalUser,
                                  textAlign: TextAlign.center,
                                  style:
                                      TextStyle(color: Colors.pink, fontSize: 60.0),
                                )),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(11.0),
                          child: Card(
                            child: ListTile(
                                title: TextButton.icon(
                                    onPressed: () {
                                      getTotalCategory();
                                    },
                                    icon: Icon(Icons.category),
                                    label: Text("Categories")),
                                subtitle: Text(
                                  category,
                                  textAlign: TextAlign.center,
                                  style:
                                      TextStyle(color: Colors.pink, fontSize: 60.0),
                                )),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(11.0),
                          child: Card(
                            child: ListTile(
                                title: TextButton.icon(
                                    onPressed: () {
                                      getTotalProduct();
                                    },
                                    icon: Icon(Icons.track_changes),
                                    label: Text("Products")),
                                subtitle: Text(
                                  '$totalProduct',
                                  textAlign: TextAlign.center,
                                  style:
                                      TextStyle(color: Colors.pink, fontSize: 60.0),
                                )),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(11.0),
                          child: Card(
                            child: ListTile(
                                title: TextButton.icon(
                                    onPressed: null,
                                    icon: Icon(Icons.tag_faces),
                                    label: Text("Sold")),
                                subtitle: Text(
                                  '13',
                                  textAlign: TextAlign.center,
                                  style:
                                      TextStyle(color: Colors.pink, fontSize: 60.0),
                                )),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(11.0),
                          child: Card(
                            child: ListTile(
                                title: TextButton.icon(
                                    onPressed: null,
                                    icon: Icon(Icons.shopping_cart),
                                    label: Text("Orders")),
                                subtitle: Text(
                                  '5',
                                  textAlign: TextAlign.center,
                                  style:
                                      TextStyle(color: Colors.pink, fontSize: 60.0),
                                )),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(11.0),
                          child: Card(
                            child: ListTile(
                                title: TextButton.icon(
                                    onPressed: null,
                                    icon: Icon(Icons.close),
                                    label: Text("Return")),
                                subtitle: Text(
                                  '0',
                                  textAlign: TextAlign.center,
                                  style:
                                      TextStyle(color: Colors.pink, fontSize: 60.0),
                                )),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )
            : Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.blueAccent),
                ),
              ));
  }
}
