import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class ShowCategoryItem extends StatefulWidget {
  const ShowCategoryItem({Key key}) : super(key: key);

  @override
  _ShowCategoryItemState createState() => _ShowCategoryItemState();
}

class _ShowCategoryItemState extends State<ShowCategoryItem> {

  @override
  void initState() {
    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder(
        stream: FirebaseFirestore.instance.collection('Categories').snapshots(),
        builder: (BuildContext context,AsyncSnapshot<QuerySnapshot> snapshot){
          if(!snapshot.hasData){
            return Text('No data');
          }
          return ListView(
            children: snapshot.data.docs.map((document){
              return Card(
                child: ListTile(
                  title: Text(document['categoryName']),),);
            }).toList(),
          );
        },
      ),
    );
  }
}
