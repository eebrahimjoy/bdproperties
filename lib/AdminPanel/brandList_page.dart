import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class ShowBrandItem extends StatefulWidget {
  const ShowBrandItem({Key key}) : super(key: key);

  @override
  _ShowBrandItemState createState() => _ShowBrandItemState();
}

class _ShowBrandItemState extends State<ShowBrandItem> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder(
        stream: FirebaseFirestore.instance.collection('Brands').snapshots(),
        builder: (BuildContext context,AsyncSnapshot<QuerySnapshot> snapshot){
          if(!snapshot.hasData){
            return Text('No data');
          }
          return ListView(
            children: snapshot.data.docs.map((document){
              return Card(

                child: ListTile(
                  title: Text(document['brandName']),),);
            }).toList(),
          );
        },
      ),
    );
  }
}
