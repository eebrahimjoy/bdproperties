 import 'package:flutter/material.dart';
import 'package:flutter_ecommerce/splashScreen.dart';
import 'package:provider/provider.dart';

import 'UserSide/View/cart/provider.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return ChangeNotifierProvider(create:(context){
      return Total();
    },

    child: MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
    ),
    );
  }
}


