import 'dart:async';

import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ecommerce/UserSide/View/homePage.dart';
import 'package:flutter_ecommerce/sharedPreference.dart';

import 'UserSide/View/login.dart';

class SplashScreen extends StatefulWidget {

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState(){
    super.initState();
    Firebase.initializeApp();
    Timer(Duration(seconds: 3),(){
      return setPref();
    });
  }

  @override
  void dispose(){
    super.dispose();
  }

  void setPref() async{
    await Prefs.loadPref();
    bool isLoggedIn = Prefs.getBool(Prefs.IS_LOGGED_IN,def: false);
    if(!isLoggedIn){
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context)=>Login()),
              (route) => false);
    }
    else{
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context)=>HomePage()),
              (route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: Colors.blue.shade50,
            image: DecorationImage(
                image: AssetImage("images/coverPage.png"),
                fit: BoxFit.cover,
                colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.1),BlendMode.dstATop)
            )
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Flexible(
                child: Hero(
                  tag: 'logo',
                  child: Icon(
                    Icons.shopping_cart,
                    size: 120,
                    color: Colors.pink,
                  ),
                ),
              ),
              SizedBox(
                height: 8.0,
              ),
              Hero(
                tag: 'HeroTitle',
                child: Text(
                  'E-Shopping',
                  style: TextStyle(
                      decoration: TextDecoration.none,
                      color: Colors.pink,
                      fontFamily: 'Poppins',
                      fontSize: 26,
                      fontWeight: FontWeight.w700),
                ),
              ),
              TyperAnimatedTextKit(
                isRepeatingAnimation: false,
                speed: Duration(milliseconds: 60),
                text: ["Your best companion for a cherished life".toUpperCase()],
                textStyle: TextStyle(
                    fontFamily: 'Poppins',
                    fontSize: 12,
                    color: Colors.pinkAccent),
              ),
              SizedBox(
                height: 40.0,
              ),
              Container(
                width: 150,
                height: 2,
                child: LinearProgressIndicator(
                  backgroundColor: Colors.pink,
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
