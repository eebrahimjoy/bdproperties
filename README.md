**Flutter E-commerce Application**

[**App Demonstration**](https://drive.google.com/drive/folders/1SnUmpX-7nZrObswzJVvD2-IfRBVDS6gP?usp=sharing)


**Functionality:** 
    - Shared Preference, Splash Screen, Sign in, Log in, Log out (Firebase Auth)
    - Admin Side (Dashboard, Product Add, Product List, Brand Add, Brand List, Category Add, Category List)
    - Admin side to User side
    - User Side (Carousel, Recent products, Products details, Favourites, Cart, Checkout, Confirm Shopping)
    - Provider (Cart, Checkout)
    - Backend (Firebase)


**External Packages:**
    - cupertino_icons: ^1.0.2
    - carousel_pro: ^1.0.0
    - cloud_firestore: ^2.4.0
    - firebase_auth: ^2.0.0
    - animated_text_kit: ^4.2.1
    - animated_background: ^2.0.0
    - firebase_core: ^1.3.0
    - modal_progress_hud: 0.1.3
    - edge_alert: ^0.0.1
    - firebase_database: ^7.1.2
    - fluttertoast: ^8.0.8
    - image_picker: ^0.8.3+2
    - firebase_storage: ^10.0.2
    - shared_preferences: ^2.0.6
    - carousel_slider: ^4.0.0
    - provider: ^6.0.0



